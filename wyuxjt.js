
/*
 * 外研u学

[rewrite_local]
https?://www.unischool.cn|h5.mailejifen.com|sync-study.unipus.cn

[mitm]
hostname = www.unischool.cn, h5.mailejifen.com, sync-study.unipus.cn
*/



var url = $request.url;
var body = $response.body;
var obj;

if (url.includes("https:/\/sync-study.unipus.cn/syncstudy/v1/users/student_vip/query")) {
    obj = JSON.parse(body);

    obj.data.data = {
        "UNISCHOOL_STUDENT_SUPER_VIP" : {
            "objectName" : "UNISCHOOL_STUDENT_SUPER_VIP",
            "objectId" : "UNISCHOOL_STUDENT_SUPER_VIP",
            "now" : "2024-08-03 17:48:05",
            "type" : 1,
            "activeType" : 0,
            "expireTime" : "2099-12-31 23:59:59"
        }
    };

    body = JSON.stringify(obj);
} else if (url.includes("www.unischool.cn/api/common/v1/account/user/student/info")) {
    obj = JSON.parse(body);
    obj.data.vip = true;
    obj.data.endTime = "2099-12-31 23:59:59";
    obj.data.usableIntegrate = 599520;
    body = JSON.stringify(obj);
} else {
    body = body.replace(/"superVip"\s*:\s*[^,}]+/g, '"superVip":true');
    body = body.replace(/"superDignityVip"\s*:\s*[^,}]+/g, '"superDignityVip":true');
    body = body.replace(/"price"\s*:\s*[^,}]+/g, '"price":0');
   body = body.replace(/"lockStatus"\s*:\s*[^,}]+/g, '"lockStatus":1');
    body = body.replace(/"downLoad"\s*:\s*[^,}]+/g, '"downLoad":true');
    body = body.replace(/"unlockTime"\s*:\s*[^,}]+/g, '"unlockTime":1726222427');
    body = body.replace(/"expireTime"\s*:\s*[^,}]+/g, '"expireTime":"2099-12-31 23:59:59"');
    body = body.replace(/"purchased"\s*:\s*[^,}]+/g, '"purchased":true');
    body = body.replace(/"canReceiveIntegrate"\s*:\s*[^,}]+/g, '"canReceiveIntegrate":9999');
    body = body.replace(/"vip"\s*:\s*[^,}]+/g, '"vip":1');
    body = body.replace(/"credits"\s*:\s*[^,}]+/g, '"credits":88888');
    body = body.replace(/"free"\s*:\s*[^,}]+/g, '"free":true');
}

$done({ body });