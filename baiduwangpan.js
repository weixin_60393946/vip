/*
百度网盘 解锁Svip在线视频倍率/清晰度

***************************
QuantumultX:

[rewrite_local]
https:\/\/pan\.baidu\.com\/rest\/\d\.\d\/membership\/user url script-response-body bdwp.js

[mitm]
hostname = pan.baidu.com
**************************/

if ($response.body) {
    $done({
        body: JSON.stringify({
  "product_infos" : [
    {
      "product_id" : "5210897752128663390",
      "end_time" : 2147483648,
      "buy_time" : "1424010948",
      "cluster" : "offlinedl",
      "status" : "0",
      "start_time" : 1424010948,
      "function_num" : 2,
      "buy_description" : "离线下载套餐(永久)",
      "product_description" : "离线下载套餐(永久)",
      "detail_cluster" : "offlinedl",
      "product_name" : "offlinedl_permanent"
    },
    //⬇️控制会员状态
    {
      "product_id" : "5310897792128633390",
      "end_time" : 2147483648,
      "buy_time" : "1424010948",
      "cluster" : "vip",
      "status" : 0,
      "start_time" : 1717171200,
      "function_num" : 4,
      "buy_description" : "超级会员",
      "product_description" : "超级会员",
      "detail_cluster" : "svip",
      "product_name" : "svip2_nd"
    }
  ],
  "center_skip_config" : {
    "action_page_size" : {
      "width" : 848,
      "height" : 648
    },
    "action_type" : 0,
    "action_url" : "https://pan.baidu.com/buy/center?tag=8"
  },
  "last_privilege_card_v2" : {

  },
  "current_privilege_card" : [

  ],
  "current_product_v2" : {

  },
  "request_id" : 377095527006514093,
  "current_privilege_card_v2" : {

  },
  "up_product_infos" : [

  ],
  "last_privilege_card" : [

  ],
   //⬇️控制会员信息
  "level_info" : {
    "history_value" : 620,
    "current_level" : 10,
    "last_manual_collection_time" : 0,
    "current_value" : 599520,
    "history_level" : 10,
    "v10_id" : ""
  },
  "user_tag" : "{\"has_buy_record\":1,\"has_vip_buy_record\":1,\"has_vipv2_buy_record\":1,\"has_svip_buy_record\":1,\"has_buy_vip_svip_record\":1,\"last_buy_record_creat_time\":1647395132,\"is_vip\":1,\"is_svip\":1,\"is_vipv2\":1,\"is_vip_v2\":1,\"last_vip_type\":1,\"last_vip_svip_end_time\":2147483648,\"last_vip_end_time\":2147483648,\"last_vipv2_end_time\":2147483648,\"last_svip_end_time\":2147483648,\"is_svip_sign\":1,\"is_vipv2_sign\":0,\"notice_user_type\":2,\"notice_user_status\":2,\"is_first_act\":0,\"is_first_charge\":0,\"is_vip_first_charge\":0,\"activateTime\":1409232945,\"last_v10_end_time\":2147483648}",
  "currenttime" : 1718328092,
  "previous_product" : {
    "detail_cluster" : "svip",
    "expired_time" : 2147483648,
    "cluster" : "vip",
    "product_type" : "svip2_1m_auto"
  },
  "current_mvip_v2" : {

  },
  "current_product" : [

  ],
  "reminder" : {
    "reminderWithContent" : [

    ],
    "advertiseContent" : [

    ]
  },
  "current_mvip" : [

  ],
  "previous_product_v2" : {
    "detail_cluster" : "svip",
    "expired_time" : 2147483648,
    "cluster" : "vip",
    "product_type" : "svip2_1m_auto"
  }
}
)
    });
} else {
    $done({});
}
